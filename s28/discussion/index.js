// CRUD Operations

// Create
// Inserting one collections
db.users.insertOne({
	"firstName": "Jnae",
	"lastName": "Doe",
	"age": 21,
	"contact": {
		"phone": "85264789514",
		"email": "janedoe@mail.com"
	},
	"course": ["CSS", "JavaScript", "Python"],
	"department": "none"
});

// Insert many collections
db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"contact": {
			"phone": "85264789514",
			"email": "stephenh@mail.com"
		},
		"course": ["React", "PHP", "Python"],
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"contact": {
			"phone": "85264789514",
			"email": "neilarms@mail.com"
		},
		"course": ["React", "Laravel", "Sass"],
		"department": "none"
	}
]);

// Finding documents (Read)
// Find

// Retrieve all documents
db.users.find();

// Retrieving single documents
db.users.find({"firstName": "Stephen"});

// Retrieving documents with multiple parameters
db.users.find({"lastName": "Armstrong", "age": 82});

// Updating documents (Update)

// Update a single document to update

// Creating a document to update
db.user.insertOne({
	"firstName": "Tes",
	"lastName": "Tes",
	"age": 0,
	"contact": {
		"phone": "0000000000",
		"email": "test@mail.com"
	},
	"course": [],
	"department": "none"
});

db.users.updateOne(
	{ "firstName": "Test" },
	{
		$set : {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"contact": {
				"phone": "12345678911",
				"email": "bill@mail.com"
			},
			"course": ["PHP", "Laravel", "HTML"],
			"department": "Operations",
			"status": "active"
		}
	}
);

db.users.updateMany(
	{ "department": "none" },
	{
		$set : {
			"department": "HR"
		}
	}
);

// Replace one
db.users.replaceOne(
	{ "firstName": "Bill"},
	{
		"firstName": "Bill",
		"lastName": "Gates",
		"age": 65,
		"contact": {
			"phone": "12345678911",
			"email": "bill@mail.com"
		},
		"course": ["PHP", "Laravel", "HTML"],
		"department": "Operations",	
	}
);

// Deleting documents (Delete)

// Creating a document to delete
db.users.insertOne({
	"firstName": "Test"
});

// Deleting a single document
db.users.deleteOne({
	"firstName": "Test"
});

// Query an embedded document
db.users.find({
	"contact": {
		"phone": "85264789514",
		"email": "neilarms@mail.com"
});