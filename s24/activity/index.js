//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator
 
function getCube(num1) {
    return result = num1 ** 3;
}

// let getCube = 2 ** 3;

// Template Literals

let num1 = 2;
console.log(`The cube of ${num1} is ${getCube(num1)}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
console.log(`I live at ${address[0]} ${address[1]}, ${address[2]}${address[3]}`)

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
let sum = 0;

numbers.forEach((number) => {
    console.log(`${number}`);
    sum += number;
})
console.log("Total acquired using forEach loop");
console.log(sum);

let reduceNumber = numbers.reduce(
  (accumulator, currentValue) => accumulator + currentValue,
  0
);
console.log("Total acquired using reduce array method");
console.log(reduceNumber);

// Javascript Classes

class Dog {
  constructor(name, age, breed) {
    this.name = name,
    this.age = age,
    this.breed = breed
  }
}

let newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}