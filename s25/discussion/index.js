// JSON Object
/*
	- JSON stands for JavaScript Object Notation
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/
// JSON as objects
/*{
	"city": "Quezon city",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Arrays
/*"cities": [
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"}
]*/
	
// Mini Activity - Create a JSON Array that will hold three breeds of dogs with properties: name, age, breed.

/*"dogs": [
	{"name": "Khulet", "age": "5", "breed": "Shitzhu"},
	{"name": "Pinggoy", "age": "7", "breed": "Shitzhu"},
	{"name": "Tom", "age": "12", "breed": "Shitzhu"}
]*/

// JSON Methods

// Convert Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X' }, { batchName: 'Batch Y'}];

// the "stringify" method is used to convert JavaScript objects into a string
console.log('Result before stringify method: ');
console.log(batchesArr);
console.log('Result from stringify method: ');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'		
	}
});

console.log(data);

// Using Stringify method with variables
// User details
let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);

// Mini Activity - Create a JSON data that will accept user car details with variables brand, type, year.

let carBrand = prompt("What is the brand of your car?");
let carModel = prompt("What is the model of your car?");
let carYear = prompt("What is the year of your car's model?");

let carData = JSON.stringify({
	carBrand: carBrand,
	carModel: carModel,
	carYear: carYear
})

console.log(carData);

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log("Result form parse method");
console.log(JSON.parse(batchesJSON));