const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({

    email : {

        type : String,
        required : [true, "Email is required"]
    },
    password : {

        type : String,
        required : [true, "Passowrd is requred"]
    },
    isAdmin : {

        type :Boolean,
        default : false
    },
    orderedProduct : [{

        products : [{
            productId : {
                type : mongoose.Schema.ObjectId
            },
            productName : {
                type : String,
                required : [true, "Product Name is requred"]
            },
            quantity : {
                type : Number,
                required : [true, "Quantity is requred"]
            }
        }],
        
        totalAmount : {
            type : Number,
            default : 0
        },
        purchasedOn : {
            type : Date,
            default : new Date()
        }
    }]
});

module.exports = mongoose.model("User", userSchema);
