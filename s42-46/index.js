const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");


const app = express();

// Connect to our MongoDB
mongoose.connect("mongodb+srv://jmvelasco2298:wz6N6WN0QequIcb2@wdc028-course-booking.0qjqyjt.mongodb.net/e-commerceAPI?retryWrites=true&w=majority",{

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error."));
db.once('open', () => console.log("Now connected to MongoDB Atlas!"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {

	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})