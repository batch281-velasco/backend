const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for User Registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for User Authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for Retrieving User Details
// Routing req and res are now embedded in the controllers
router.get("/details", userController.getProfile);

// Route for Creating Order
// Routing req and res are now embedded in the controllers
router.post('/checkout', userController.checkout);


module.exports = router;