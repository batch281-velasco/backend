const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for Creating a Product (Admin Only)
router.post("/", auth.verify, (req, res) => {
	
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});

// Route for Retrieving All Products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// Route for Retrieving All Active Products
router.get("/active", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// Route for Retrieving a Specific Product
router.get("/:productId", (req, res) => {

	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for Updating a Product (Admin Only)
router.put("/:productId", auth.verify, (req, res) => {

    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
});

// Route for Archiving a Product (Admin Only)
router.patch("/archive/:productId", auth.verify, (req, res) => {

	const data = {

			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		if(data.isAdmin == true){
			
			productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
		} else {
			
			res.send(false);
		}

});

// Route for Activating a Product (Admin Only)
router.patch("/activate/:productId", auth.verify, (req, res) => {

	const data = {

			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		if(data.isAdmin == true){
			
			productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
		} else {
			
			res.send(false);
		}

});

module.exports = router;