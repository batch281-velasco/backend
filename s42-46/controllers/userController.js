const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error) => {

		if(error){

			return false;
		}
		else{

			return true;
		}
	})
};

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false
		}
		else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			}
			else{

				return false;
			}
		}
	});
};

// Retrieving details of the user
module.exports.getProfile = (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	return User.findById(userData.id).then(result => {
		result.password = ""
		return res.send(result)
	})
	.catch(err => res.send(err))
};


// Checking out an order (For non-Admin users)
module.exports.checkout = async (req, res) => {
	const { productId, quantity } = req.body;
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == true) {
		res.send("You're an Admin. Please use a non-Admin user to order a Product.");
	} else {
		// Order
		// Order schema for Postman, productName causes error
		/*
			{
				"productId" : product._id,
				"quantity" : number
			}
		*/
		let orderData = {

			userId: userData.id,
			productId,
			quantity
		};

		let product = await Product.findById(orderData.productId);

		if (product.isActive == true) {
			product.userOrders.push({ userId: orderData.userId });

			await product.save();

			//Checkout Details
			let user = await User.findById(orderData.userId);

			console.log(user);

			let newOrderedProduct = {

				products: [{

					productId: orderData.productId,
					productName: product.name,
					quantity: orderData.quantity
				}],
				totalAmount: product.price * orderData.quantity,
				purchasedOn: new Date()
			};

			user.orderedProduct.push(newOrderedProduct);

			await user.save();

			res.send("Ordered successfully");
		} else {
			res.send("Product out of stock. Please select other product");
		}		
	}
};
