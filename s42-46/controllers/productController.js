const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create Product (for Admin only)
module.exports.addProduct = (data) => {

	let newProduct = new Product({

		name : data.product.name,
        description : data.product.description,
        price : data.product.price
	});

	return newProduct.save().then((product, error) => {

		if(error){

			return false;
		}
		else{

			return true;
		};
	})
};

// Retrieve All Products
module.exports.getAllProducts = () => {

	return Product.find({}).then(result => {

		return result;
	});
};

// Retrieve All Active Products
module.exports.getAllActiveProducts = () => {

	return Product.find({isActive : true}).then((result) => {

    	return result;
  });
};

// Retrieve A Specific Product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result;
	});
};

// Update A Product (Admin Only)
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {

		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {

		if(error){

			return false;
		} else {

			return true;
		}
	});
};

// Archive A Product (Admin Only)
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;
		} else {

			return true;
		}

	});
};

// Activating A Product (Admin Only)
module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		if (error) {

			return false;
		} else {

			return true;
		}

	});
};