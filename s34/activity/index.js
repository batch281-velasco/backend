const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});

let users = [];

app.post("/users", (req, res) => {
	if(req.body.username !=='' && req.body.password !=='') {
		users.push(req.body);
		res.send(`Users ${req.body.username} successfully registered`);
	}
	else {
		res.send("Please input BOTH username and password");
	}
	console.log(users);
});

app.get("/users", (req, res) => {
	res.send(users);
})

app.delete("/delete-users", (req, res) => {
	let message;
	if(users.length > 0) {
		for (let i = 0; i < users.length; i++) {
				if(req.body.username == users[i].username){
					users.splice(i, 1);
					message = `User ${req.body.username} has been deleted.`;
					break;
				}
				else {
					message = "No users found.";
				}
		}
		if (message == undefined) {
			message = "User does not exist.";
		} 
	}
	res.send(message);
	console.log(users);
})

if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port: ${port}`));
}

module.exports = app;
