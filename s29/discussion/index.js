// [Section] Comparison Query Operators

// $gt / $gte
/*
	- allows us to find documents that have field number values greater than or equal to a specified field
	- Syntax:
		db.collectionName.find({field: {$gt: value} });
		db.collectionName.find({field: {$gte: value} });
*/
db.users.find({ age: {$gt: 50}});
db.users.find({ age: {$gte: 21}});

// $lt / $lte
/*
	- allows us to find documents that have field number values less than or equal to a specified field
	- Syntax:
		db.collectionName.find({field: {$lt: value} });
		db.collectionName.find({field: {$lte: value} }); 
*/
db.users.find({ age: {$lt: 50}});
db.users.find({ age: {$lte: 65}});

// $ne operator
/*
	- allows us to find documents that have field numbers not equal to specified value.
	- Syntax:
		db.collectionName.find({field: {$ne: value} });
*/
db.users.find({ age: {$ne: 82}});

// $in operator
/*
	- allows us to find documents with specific match criteria one field using different values
	- Syntax:
		db.collectionName.find({field: {$in: value} });
*/
db.users.find({ lastName: {$in: ["Hawking", "Doe"] } });
db.users.find({ course: {$in: ["HTML", "React"] } });

// Opposite of in/$in is not in/$nin
db.users.find({ course: {$nin: ["HTML", "React"] } });

// [Section] Logical Query Operators

// $or operator
/*
	- allows us to fid documents that match a single criteria from multiple provided search criteria
	- Syntax:
		db.collectionName.find({ $or: [{ fieldA: valueB },{ fieldB: valueC }] });
*/
db.users.find({ $or: [{ firstName: "Neil" },{ age: 21 }] });
db.users.find({ $or: [{ firstName: "Neil" },{ age: {$gt: 30}} ] });

// $and operator
/*
	- allows us to find documents matching mutliple criteria in a single field
	- Syntax:
		db.collectionName.find({ $and: [{ fieldA: valueB },{ fieldB: valueC }] });
*/
db.users.find({ $and: [{ age: {$ne: 82} },{ age: {$ne: 76} }] });

// Field Projection
/*
	- retrieving documents are common operations that we do and by default MongoDB queries return the whol document as a response
	- when delaign with complex data structures, there mightbe instances when fields are not useful for the query that w are trying to accomplish
	- to help with readability of the values returned, we can include/exclude fields from the response
*/

// Inclusion
/*
	- allows us to include/add specific fields only when retrieving documents
	- the value provided is 1 to denote that the field is being included
	- Syntax:
		db.collectionName.find({{criteria}, {field: 1}});
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// Exclusiong
/*
	- allows us to exclude.remove specific fields only when retrieving documents
	- the value provided is 0 to denote that the filed is being included
	- Syntax:
		db.collectionName.find({{criteria}, {field: 0}});
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
);

// Supressing the ID Field
db.users.find(
	{
		firstName: "Jane"
	},
	{
		_id: 0,
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// Returning specific fields in Embedded documents
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1,
	}
);

// Suppressing Specific Fields in embedded documents
db.users.find(
	{
		firstName: "Jane"
	},
	{		
		"contact.phone": 0,
	}
)

// Project Specific Array Elements in Returned Array
/*
	- the $slice operator allows us to retrieve only 1 element that matches the search criteria
*/

// [Section] Evaluation Query Operators

// $regex
/*
	- allows to us to find documents that match a specific string pattern using regular expression
	- Syntax:
		db.collectionName.find({field: $regex: "pattern", $options: "$optionValue"});
*/

// Case sensitive query
db.users.find({firstName: {$regex: "N"}});
db.users.find({firstName: {$regex: "n"}});

// Case insensitive query
db.users.find({firstName: { $regex: 'j', $options: 'i'}})