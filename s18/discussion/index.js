// Function with parameters

function printName(name){
	console.log("My name is " + name);
}

printName("Juana");
printName("John");

let sampleVariable = "Bella";
printName(sampleVariable);

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);

// Function as Arguments
function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

// Using multiple parameters
function creatFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

creatFullName("Juan", "Perez", "Dela Cruz", "Hello");

// Using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

creatFullName(firstName, middleName, lastName);

// Return statement - allows us to output a value from a function to be passed to the line/block of code that invoked the function

function returnFullname(firstname, middleName, lastName){
	return firstname + " " + middleName + " " + lastName;
	console.log("This message will not be printed");
}

let completeName = returnFullname("Jeffrey", "Smith", "Bezos");
console.log(completeName);
console.log(returnFullname(firstName, middleName, lastName));

function returnAddress(city, country){
	let fullAddress = city + ', ' + country;
	return fullAddress;
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);